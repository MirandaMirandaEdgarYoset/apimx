var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var movimientosv2JSON=require('./movimientosv2.json');
//var bodyparser=require('body-parser');
//app.use(bodyparser.json());

app.listen(port);

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use((req,res,next)=>{
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
})
var requestjson = require('request-json');
var urlMlabRaiz ="https://api.mlab.com/api/1/databases/emiranda/collections";
var apiKeyMlab ="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlabRaiz;
var urlClientes ="https://api.mlab.com/api/1/databases/emiranda/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab=requestjson.createClient(urlClientes);

console.log('todo list RESTful API server started on: ' + port);

app.get('/',(req,res)=>{
    res.sendFile(path.join(__dirname,'index.html'));
});
app.post('/',(req,res)=>{
    res.send('Hola hemos recibido su peticion post');
});
app.put('/',(req,res)=>{
    res.send('Hola hemos recibido su peticion put');
});
app.delete('/',(req,res)=>{
    res.send('Hola hemos recibido su peticion delete cambiada');
});

app.get('/clientes/:idcliente',(req,res)=>{
    res.send('Aqui tiene al cliente: '+req.params.idcliente);
});

app.get('/v1/Movimientos',(req,resp)=>{
  resp.sendfile('movimientosv1.json');
});

app.get('/v2/Movimientos',(req,resp)=>{
  resp.json(movimientosv2JSON);
});

app.get('/v2/Movimientos/:id',(req,resp)=>{
  resp.send(movimientosv2JSON[req.params.id]);
});

app.get('/v2/Movimientosquery', function(req, res) {
  console.log(req.query);
  res.send('Recibido: ' + req.query.nombre);
})

app.post('/v2/Movimientos',(req,resp)=>{
  let nuevo = req.body;
  nuevo.id=movimientosv2JSON.length+1;
  movimientosv2JSON.push(nuevo);
  resp.send('Movimiento dado de alta con id: '+nuevo.id);
})

app.get('/Clientes',(req,resp)=>{
  clienteMlab.get('',function (err,resM,body){
    if(err){
      console.log(body);
    }else{
      resp.send(body);
    }
  })
})

app.post('/Clientes',(req,resp)=>{
  clienteMlab.post('',req.body,function(err,resM,body){
    resp.send(body);
  })
})

app.post('/login',(req,resp)=>{
  resp.set('Access-Control-Allow-Headers','Content-Type');
  var email = req.body.email;
  var password = req.body.password;
  var query ='q={"email":"'+email+'","password":"'+password+'"}'
  console.log(query);

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz+"/Usuarios?"+apiKeyMlab+"&"+query);
  console.log(urlMlabRaiz+"/Usuarios?"+apiKeyMlab+"&"+query);

  clienteMlabRaiz.get('',(err,resM,body)=>{
    if(!err){
      if(body.length==1){
        resp.status(200).send('Usuario Logado');
      }
      else{
        resp.status(400).send('Usuario no encontrado');
      }
    }
  })
})
